FROM node:18-alpine

# RUN export GOLANG_VERSION=1.20.1
RUN apk update && apk add --no-cache git go gcc bash musl-dev openssl-dev ca-certificates && update-ca-certificates
# RUN wget https://dl.google.com/go/go1.20.1.src.tar.gz && tar -C /usr/local -xzf go1.20.1.src.tar.gz
# RUN cd /usr/local/go/src && ./make.bash
# RUN export PATH=$PATH:/usr/local/go/bin
# RUN cd $CI_PROJECT_DIR
# RUN rm go1.20.1.src.tar.gz
# RUN apk del go
RUN go version

ENTRYPOINT [ ]